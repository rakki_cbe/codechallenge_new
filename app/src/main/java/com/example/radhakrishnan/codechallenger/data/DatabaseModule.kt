package com.example.radhakrishnan.codechallenger.data

import android.content.Context
import com.example.radhakrishnan.codechallenger.di.ApplicationModule
import dagger.Module
import dagger.Provides

@Module(includes = [ApplicationModule::class])
class DatabaseModule {


    @Provides
    fun provideEventDao(context: Context): EventDao? {
        return EventDatabase.getInstance(context)?.getEventDao()
    }

    @Provides
    fun provideMeetingDao(context: Context): MeetingDao? {
        return EventDatabase.getInstance(context)?.getMeetingDao()
    }
}