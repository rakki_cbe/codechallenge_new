package com.example.radhakrishnan.codechallenger.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "meeting")
data class Meeting(@ColumnInfo(name = "location") var location: String,
                   @PrimaryKey(autoGenerate = false) var uuid: String,
                   @ColumnInfo(name = "meetingWith") var meeting_with: String,
                   @ColumnInfo(name = "eventId") var event_uuid: String) {
    constructor() : this("", "", "", "")
}