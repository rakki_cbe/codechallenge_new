package com.example.radhakrishnan.codechallenger.welcome.di


import com.example.radhakrishnan.codechallenger.welcome.WelcomeActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(WelcomeModule::class))
interface WelcomeComponent {
    fun injectMyPresenter(mainActivity: WelcomeActivity)
}