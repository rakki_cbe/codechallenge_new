package com.example.radhakrishnan.codechallenger.meeting

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.radhakrishnan.codechallenger.R
import com.example.radhakrishnan.codechallenger.data.model.Meeting
import javax.inject.Inject


class Adapter @Inject constructor() : RecyclerView.Adapter<ViewHolderCustom>() {
    var item: List<Meeting> = arrayListOf()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderCustom {
        val inflater = LayoutInflater.from(
                p0.context)
        val v = inflater.inflate(R.layout.row_meeting_item, p0, false)
        // set the view's size, margins, paddings and layout parameters
        return ViewHolderCustom(v)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(p0: ViewHolderCustom, p1: Int) {
        p0.setValue(item.get(p1))
    }
}

class ViewHolderCustom(val view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView

    init {
        title = view.findViewById(R.id.name)

    }

    fun setValue(item: Meeting) {
        this.title.text = item.meeting_with

    }

}
