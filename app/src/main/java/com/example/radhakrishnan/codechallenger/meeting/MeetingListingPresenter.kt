package com.example.radhakrishnan.codechallenger.meeting

import com.example.radhakrishnan.codechallenger.BasePresenter
import com.example.radhakrishnan.codechallenger.BaseView
import com.example.radhakrishnan.codechallenger.data.MeetingDao
import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.data.model.Meeting
import com.example.radhakrishnan.codechallenger.event.MeetingCallBack
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MeetingListingPresenter @Inject constructor(val view: MeetingView) : BasePresenter(view), MeetingCallBack {
    fun getEventListingData(id: String) {
        view.showProgress()
        apiHelper.getMeetingListFromDb(id, view.getDataBaseComponent()?.getMeetingDao(),
                object : Observer<List<Meeting>> {
                    override fun onComplete() {
                        view.hidProgress()
                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: List<Meeting>) {
                        view.meetingData(t)
                    }

                    override fun onError(e: Throwable) {
                        view.hidProgress()
                    }

                })

    }

    fun getMeetingInfo(item: Event) {
        view.showProgress()
        // apiHelper.getMeetingListFromDb(view.getDataBaseComponent().getMeetingDao(),this)

    }

    override fun getMeetingDao(): MeetingDao? {
        return view.getDataBaseComponent()?.getMeetingDao()
    }


    override fun meetingDataSavedToDb() {
        view.hidProgress()
        view.dataUpdateSuccessfully()
    }

    override fun error(e: Throwable) {
        view.hidProgress()
    }


}

interface MeetingCallBack {
    fun meetingDataSavedToDb()
    fun getMeetingDao(): MeetingDao?
    fun error(e: Throwable)

}

interface MeetingView : BaseView {
    fun meetingData(list: List<Meeting>)
    fun dataUpdateSuccessfully()


}