package com.example.radhakrishnan.codechallenger.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event")
data class Event(@ColumnInfo(name = "eventName") var event_system_name: String,
                 @ColumnInfo(name = "location") var location: String,
                 @PrimaryKey(autoGenerate = false) var uuid: String) {
    constructor() : this("", "", "")

}