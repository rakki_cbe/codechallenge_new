package com.example.radhakrishnan.codechallenger.di

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule constructor(var context: Context) {
    @Provides
    fun getApplicationContext(): Context {
        return context
    }
}