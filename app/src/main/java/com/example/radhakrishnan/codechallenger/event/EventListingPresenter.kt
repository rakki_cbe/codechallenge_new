package com.example.radhakrishnan.codechallenger.event

import com.example.radhakrishnan.codechallenger.BasePresenter
import com.example.radhakrishnan.codechallenger.BaseView
import com.example.radhakrishnan.codechallenger.data.MeetingDao
import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.meeting.MeetingCallBack
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class EventListingPresenter @Inject constructor(val view: EventView) : BasePresenter(view), MeetingCallBack {
    var item: Event? = null
    fun getEventListingData() {
        view.showProgress()
        apiHelper.getEventListFromDb(view.getDataBaseComponent()?.getEventDao(),
                object : Observer<List<Event>> {
                    override fun onComplete() {
                        view.hidProgress()
                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: List<Event>) {
                        view.eventData(t)
                    }

                    override fun onError(e: Throwable) {
                        view.hidProgress()
                    }

                })

    }

    fun getMeetingInfo(item: Event) {
        view.showProgress()
        this.item = item
        if (view.checkInternetConnection())
            disposable.add(apiHelper.getMeetingList(item.event_system_name, this))
        else {
            view.noInterNetConnection()
            view.hidProgress()
        }

    }

    override fun getMeetingDao(): MeetingDao? {
        return view.getDataBaseComponent()?.getMeetingDao()
    }


    override fun meetingDataSavedToDb() {
        view.hidProgress()
        view.dataUpdateSuccessfully()
    }

    override fun error(e: Throwable) {
        view.hidProgress()
    }


}

interface MeetingCallBack {
    fun meetingDataSavedToDb()
    fun getMeetingDao(): MeetingDao?
    fun error(e: Throwable)

}

interface EventView : BaseView {
    fun eventData(list: List<Event>)
    fun dataUpdateSuccessfully()


}