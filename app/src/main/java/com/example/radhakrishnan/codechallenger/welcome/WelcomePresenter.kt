package com.example.radhakrishnan.codechallenger.welcome

import com.example.radhakrishnan.codechallenger.BasePresenter
import com.example.radhakrishnan.codechallenger.BaseView
import com.example.radhakrishnan.codechallenger.data.EventDao
import com.example.radhakrishnan.codechallenger.data.MeetingDao

class WelcomePresenter constructor(val view: WelcomeView) : BasePresenter(view), eventCallBack {
    override fun getMeetingDao(): MeetingDao? {
        return view.getDataBaseComponent()?.getMeetingDao()
    }

    override fun getEventDao(): EventDao? {
        return view.getDataBaseComponent()?.getEventDao()
    }

    override fun eventDataSavedToDb() {
        view.hidProgress()
        view.dataUpdateSuccessfully()
    }

    override fun error(e: Throwable) {
        view.hidProgress()
    }

    fun initiateSetup() {
        if (view.checkInternetConnection()) {
            view.showProgress()
            // eventDao=view.getDataBaseComponent()?.getEventDao()
            disposable.add(apiHelper.getEventList(this))
        } else {
            view.noInterNetConnection()
        }
    }

}

interface eventCallBack {
    fun eventDataSavedToDb()
    fun getEventDao(): EventDao?
    fun getMeetingDao(): MeetingDao?
    fun error(e: Throwable)

}

interface WelcomeView : BaseView {
    fun dataUpdateSuccessfully()

}