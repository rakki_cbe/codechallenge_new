package com.example.radhakrishnan.codechallenger

import android.app.Application
import com.example.radhakrishnan.codechallenger.di.ApplicationComponent
import com.example.radhakrishnan.codechallenger.di.ApplicationModule
import com.example.radhakrishnan.codechallenger.di.DaggerApplicationComponent


class EventApplication : Application() {
    companion object {
        var myWelcomeComponent: ApplicationComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        myWelcomeComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()

    }
}