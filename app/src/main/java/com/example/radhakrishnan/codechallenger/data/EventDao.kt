package com.example.radhakrishnan.codechallenger.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

import com.example.radhakrishnan.codechallenger.data.model.Event

@Dao
interface EventDao {
    @Query("select * from event")
    fun getEventList(): List<Event>

    @Insert(onConflict = REPLACE)
    fun storeData(data: List<Event>)

    @Query("Delete from event")
    fun deleteAll()

}