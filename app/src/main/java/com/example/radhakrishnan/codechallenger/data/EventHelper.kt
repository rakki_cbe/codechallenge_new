package com.example.radhakrishnan.codechallenger.data

import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.data.model.Meeting
import com.example.radhakrishnan.codechallenger.data.network.ApiContract
import com.example.radhakrishnan.codechallenger.meeting.MeetingCallBack
import com.example.radhakrishnan.codechallenger.welcome.eventCallBack
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class EventHelper {
    val api: Retrofit
    val apiContract: ApiContract

    @Inject
    constructor(api: Retrofit) {
        this.api = api
        apiContract = api.create(ApiContract::class.java)
    }

    fun getEventList(listener: eventCallBack):
            DisposableObserver<List<Event>> {
        return apiContract.listEvent().subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribeWith(object : DisposableObserver<List<Event>>() {
            override fun onComplete() {
                //view.hidProgress()
            }

            override fun onNext(t: List<Event>) {
                storeDataToDb(listener, t, listener.getEventDao())
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                Observable.create(object : ObservableOnSubscribe<List<Meeting>> {
                    override fun subscribe(emitter: ObservableEmitter<List<Meeting>>) {
                        emitter.onError(Throwable())
                    }
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : Observer<List<Meeting>> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: List<Meeting>) {
                    }

                    override fun onError(e: Throwable) {
                        listener.error(Throwable())
                    }

                })

                //view.hidProgress()
            }

        })
    }

    fun getEventListFromDb(eventDao: EventDao?, observer: Observer<List<Event>>) {
        Observable.create(object : ObservableOnSubscribe<List<Event>> {
            override fun subscribe(emitter: ObservableEmitter<List<Event>>) {
                if (eventDao != null) {
                    emitter.onNext(eventDao.getEventList())
                    emitter.onComplete()
                } else
                    emitter.onError(Throwable())
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(observer)

    }

    fun storeDataToDb(listener: eventCallBack, event: List<Event>, eventDao: EventDao?) {
        Observable.create(object : ObservableOnSubscribe<Boolean> {
            override fun subscribe(emitter: ObservableEmitter<Boolean>) {
                eventDao?.deleteAll()
                eventDao?.storeData(event)
                emitter.onComplete()
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : Observer<Boolean> {
            override fun onComplete() {
                listener.eventDataSavedToDb()
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Boolean) {
            }

            override fun onError(e: Throwable) {
                listener.error(e)
            }

        })
    }

    fun getMeetingList(name: String, listener: MeetingCallBack):
            DisposableObserver<List<Meeting>> {
        return apiContract.getMeetingDetail(name).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribeWith(object : DisposableObserver<List<Meeting>>() {
            override fun onComplete() {
                //view.hidProgress()
            }

            override fun onNext(t: List<Meeting>) {

                storeDataToDb(listener, t, listener.getMeetingDao(), name)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                Observable.create(object : ObservableOnSubscribe<List<Meeting>> {
                    override fun subscribe(emitter: ObservableEmitter<List<Meeting>>) {
                        emitter.onError(Throwable())
                    }
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : Observer<List<Meeting>> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: List<Meeting>) {
                    }

                    override fun onError(e: Throwable) {
                        listener.error(Throwable())
                    }

                })


            }

        })
    }

    fun getMeetingListFromDb(id: String, meetingDao: MeetingDao?, observer: Observer<List<Meeting>>) {
        Observable.create(object : ObservableOnSubscribe<List<Meeting>> {
            override fun subscribe(emitter: ObservableEmitter<List<Meeting>>) {
                if (meetingDao != null) {
                    emitter.onNext(meetingDao.getMeetingList(id))
                    emitter.onComplete()
                } else
                    emitter.onError(Throwable())
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(observer)

    }

    fun storeDataToDb(listener: MeetingCallBack, meeting: List<Meeting>, meetingDao: MeetingDao?, name: String) {
        Observable.create(object : ObservableOnSubscribe<Boolean> {
            override fun subscribe(emitter: ObservableEmitter<Boolean>) {
                meetingDao?.deleteMeeting(name)
                meetingDao?.storeData(meeting)
                emitter.onComplete()
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : Observer<Boolean> {
            override fun onComplete() {
                listener.meetingDataSavedToDb()
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Boolean) {
            }

            override fun onError(e: Throwable) {
                listener.error(e)
            }

        })
    }

    fun getMeetingList(eventName: String, listener: DisposableObserver<List<Meeting>>, schedulers: Scheduler):
            DisposableObserver<List<Meeting>> {
        return apiContract.getMeetingDetail(eventName).subscribeOn(Schedulers.io()).observeOn(schedulers).subscribeWith(listener)
    }

}