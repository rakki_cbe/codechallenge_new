package com.example.radhakrishnan.codechallenger.meeting

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.example.radhakrishnan.codechallenger.EventApplication
import com.example.radhakrishnan.codechallenger.R
import com.example.radhakrishnan.codechallenger.data.DataBaseComponent
import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.data.model.Meeting
import com.example.radhakrishnan.codechallenger.data.network.NetworkComponent
import com.example.radhakrishnan.codechallenger.event.EventListing
import kotlinx.android.synthetic.main.list_layout.*
import javax.inject.Inject


class MeetingListing : AppCompatActivity(), MeetingView, com.example.radhakrishnan.codechallenger.meeting.AdapterOnclickListener {
    var eventName: String? = null
    @Inject
    lateinit var adapter: Adapter
    @Inject
    lateinit var presenter: MeetingListingPresenter

    override fun showError() {
        Toast.makeText(this, R.string.somethingWentWrong, Toast.LENGTH_LONG).show()
    }

    override fun dataUpdateSuccessfully() {

    }

    override fun onClicked(item: Event?) {
        if (item != null)
            presenter.getMeetingInfo(item)
        else
            Toast.makeText(this, getString(R.string.somethingWentWrong), Toast.LENGTH_LONG).show()

    }

    override fun meetingData(list: List<Meeting>) {
        adapter.item = list
        adapter.notifyDataSetChanged()
    }

    override fun noInterNetConnection() {
        Toast.makeText(this, getString(R.string.NeedInterNetToUpdateDate), Toast.LENGTH_LONG).show()
        startActivity(EventListing.getActivityIntent(this))
    }

    override fun checkInternetConnection(): Boolean {
        return isNetworkConnected()
    }

    override fun getDataBaseComponent(): DataBaseComponent? {
        return EventApplication.myWelcomeComponent?.getDataBaseComponent()
    }

    override fun getNetWorkComponent(): NetworkComponent? {
        return EventApplication.myWelcomeComponent?.getNetworkComponent()
    }

    override fun showProgress() {
        customProgressbar.visibility = View.VISIBLE
    }

    override fun hidProgress() {
        customProgressbar.visibility = View.GONE
    }

    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_layout)
        eventName = getEventId()
        EventApplication.myWelcomeComponent?.getMeetingListComponent(MeetingModule(this))?.injectData(this)
        list.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        list.layoutManager = layoutManager
        list.adapter = adapter
        if (eventName != null)
            presenter.getEventListingData(eventName!!)
        else {
            finish()
            Toast.makeText(this, getString(R.string.somethingWentWrong), Toast.LENGTH_LONG)
                    .show()
        }
    }

    fun getEventId(): String? {
        return intent.extras.getString("eventId")
    }

    companion object {
        fun getActivityIntent(context: Context, id: String?): Intent {
            val intent = Intent(context, MeetingListing::class.java)
            intent.putExtra("eventId", id)
            return intent
        }
    }
}

interface AdapterOnclickListener {
    fun onClicked(item: Event?)
}
