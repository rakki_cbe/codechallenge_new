package com.example.radhakrishnan.codechallenger.data.network

import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.data.model.Meeting
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiContract {
    @GET("events")
    fun listEvent(): Observable<List<Event>>

    @GET("{event_name}")
    fun getMeetingDetail(@Path("event_name") name: String): Observable<List<Meeting>>
}