package com.example.radhakrishnan.codechallenger.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.example.radhakrishnan.codechallenger.data.model.Meeting

@Dao
interface MeetingDao {
    @Query("select * from meeting where eventId=:eventId")
    fun getMeetingList(eventId: String): List<Meeting>

    @Insert(onConflict = REPLACE)
    fun storeData(data: List<Meeting>)

    @Query("Delete from meeting")
    fun deleteAll()

    @Query("Delete from meeting where eventId=:name")
    fun deleteMeeting(name: String)
}