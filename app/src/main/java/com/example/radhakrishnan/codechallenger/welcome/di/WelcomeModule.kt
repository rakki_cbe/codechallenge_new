package com.example.radhakrishnan.codechallenger.welcome.di

import com.example.radhakrishnan.codechallenger.data.EventHelper
import com.example.radhakrishnan.codechallenger.welcome.WelcomePresenter
import com.example.radhakrishnan.codechallenger.welcome.WelcomeView
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class WelcomeModule constructor(var welcomeView: WelcomeView) {

    @Provides
    fun getWelcomePresenter(): WelcomePresenter {
        return WelcomePresenter(welcomeView)
    }

    @Provides
    fun getApiHelper(retrofit: Retrofit): EventHelper {
        return EventHelper(retrofit)
    }
}