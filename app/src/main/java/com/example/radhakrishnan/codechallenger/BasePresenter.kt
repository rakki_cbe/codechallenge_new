package com.example.radhakrishnan.codechallenger

import com.example.radhakrishnan.codechallenger.data.DataBaseComponent
import com.example.radhakrishnan.codechallenger.data.EventHelper
import com.example.radhakrishnan.codechallenger.data.network.NetworkComponent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BasePresenter {
    val disposable: CompositeDisposable = CompositeDisposable()
    val baseView: BaseView
    @Inject
    lateinit var apiHelper: EventHelper

    constructor(view: BaseView) {
        this.baseView = view
        baseView.getNetWorkComponent()?.injectMyPresenter(this)

    }

    fun onStart() {

    }

    fun onResume() {

    }

    fun onPause() {

    }

    fun onStop() {

    }

    fun onCreate() {

    }

    fun onDestroy() {
        disposable.clear()
    }


}

open interface BaseView {
    fun getNetWorkComponent(): NetworkComponent?
    fun getDataBaseComponent(): DataBaseComponent?
    fun showProgress()
    fun hidProgress()
    fun checkInternetConnection(): Boolean
    fun noInterNetConnection()
    fun showError()

}