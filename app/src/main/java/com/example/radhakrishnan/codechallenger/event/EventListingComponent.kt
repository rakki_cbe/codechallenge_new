package com.example.radhakrishnan.codechallenger.event

import com.example.radhakrishnan.codechallenger.data.EventHelper
import com.example.radhakrishnan.codechallenger.meeting.Adapter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit

@Subcomponent(modules = arrayOf(EventModule::class))
interface EventListingComponent {
    fun injectData(evet: EventListing)
}

@Module
class EventModule constructor(val event: EventView) {
    @Provides
    fun getEventPresenter(): EventListingPresenter {
        return EventListingPresenter(event)
    }

    @Provides
    fun getApiHelper(retrofit: Retrofit): EventHelper {
        return EventHelper(retrofit)
    }

    @Provides
    fun getEventAdapter(): Adapter {
        return Adapter()
    }
}
