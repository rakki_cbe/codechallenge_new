package com.example.radhakrishnan.codechallenger.event

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.radhakrishnan.codechallenger.R
import com.example.radhakrishnan.codechallenger.data.model.Event
import javax.inject.Inject


class Adapter @Inject constructor() : RecyclerView.Adapter<ViewHolderCustom>() {
    var item: List<Event> = arrayListOf()
    var listener: AdapterOnclickListener? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderCustom {
        val inflater = LayoutInflater.from(
                p0.context)
        val v = inflater.inflate(R.layout.row_event_item, p0, false)
        // set the view's size, margins, paddings and layout parameters
        return ViewHolderCustom(v, listener)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(p0: ViewHolderCustom, p1: Int) {
        p0.setValue(item.get(p1))
    }
}

class ViewHolderCustom(val view: View, val listener: AdapterOnclickListener?) : RecyclerView.ViewHolder(view) {
    val title: TextView
    var click: Click

    init {
        title = view.findViewById(R.id.name)
        click = Click(listener)
        view.setOnClickListener(click)
    }

    fun setValue(item: Event) {
        click.event = item
        this.title.text = item.event_system_name

    }

}

class Click(val listener: AdapterOnclickListener?) : View.OnClickListener {
    var event: Event? = null
    override fun onClick(v: View?) {
        listener?.onClicked(event)
    }

}