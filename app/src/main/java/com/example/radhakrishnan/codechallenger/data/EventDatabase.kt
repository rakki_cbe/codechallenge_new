package com.example.radhakrishnan.codechallenger.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.radhakrishnan.codechallenger.data.model.Event
import com.example.radhakrishnan.codechallenger.data.model.Meeting

@Database(entities = arrayOf(Event::class, Meeting::class), version = 1)
abstract class EventDatabase : RoomDatabase() {
    abstract fun getEventDao(): EventDao
    abstract fun getMeetingDao(): MeetingDao

    companion object {
        private var INSTANCE: EventDatabase? = null

        fun getInstance(context: Context): EventDatabase? {
            if (INSTANCE == null) {
                synchronized(EventDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            EventDatabase::class.java, "event.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}