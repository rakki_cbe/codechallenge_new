package com.example.radhakrishnan.codechallenger.data

import dagger.Subcomponent

@Subcomponent(modules = arrayOf(DatabaseModule::class))
interface DataBaseComponent {
    fun getEventDao(): EventDao?
    fun getMeetingDao(): MeetingDao?
}