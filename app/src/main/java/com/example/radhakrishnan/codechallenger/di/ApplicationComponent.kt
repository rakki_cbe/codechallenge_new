package com.example.radhakrishnan.codechallenger.di

import android.content.Context
import com.example.radhakrishnan.codechallenger.data.DataBaseComponent
import com.example.radhakrishnan.codechallenger.data.network.NetworkComponent
import com.example.radhakrishnan.codechallenger.event.EventListingComponent
import com.example.radhakrishnan.codechallenger.event.EventModule
import com.example.radhakrishnan.codechallenger.meeting.MeetingListingComponent
import com.example.radhakrishnan.codechallenger.meeting.MeetingModule
import com.example.radhakrishnan.codechallenger.welcome.di.WelcomeComponent
import com.example.radhakrishnan.codechallenger.welcome.di.WelcomeModule
import dagger.Component


@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun getWelcomeComponent(welcomeModule: WelcomeModule): WelcomeComponent
    fun getEventListingComponent(eventModule: EventModule): EventListingComponent
    fun getMeetingListComponent(meeting: MeetingModule): MeetingListingComponent
    fun getNetworkComponent(): NetworkComponent
    fun getContext(): Context
    fun getDataBaseComponent(): DataBaseComponent

}
