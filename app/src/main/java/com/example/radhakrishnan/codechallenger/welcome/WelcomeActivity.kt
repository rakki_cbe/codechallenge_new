package com.example.radhakrishnan.codechallenger.welcome


import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.example.radhakrishnan.codechallenger.EventApplication
import com.example.radhakrishnan.codechallenger.R
import com.example.radhakrishnan.codechallenger.data.DataBaseComponent
import com.example.radhakrishnan.codechallenger.data.network.NetworkComponent
import com.example.radhakrishnan.codechallenger.event.EventListing
import com.example.radhakrishnan.codechallenger.welcome.di.WelcomeModule
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class WelcomeActivity : AppCompatActivity(), WelcomeView {
    @Inject
    lateinit var presenter: WelcomePresenter

    override fun showError() {
        Toast.makeText(this, R.string.somethingWentWrong, Toast.LENGTH_LONG).show()
    }

    override fun dataUpdateSuccessfully() {
        Toast.makeText(this, getString(R.string.EventUpdated), Toast.LENGTH_LONG).show()
        startActivity(EventListing.getActivityIntent(this))
    }

    override fun noInterNetConnection() {
        Toast.makeText(this, getString(R.string.NeedInterNetToUpdateDate), Toast.LENGTH_LONG).show()
        startActivity(EventListing.getActivityIntent(this))
    }

    override fun checkInternetConnection(): Boolean {
        return isNetworkConnected()
    }

    override fun getDataBaseComponent(): DataBaseComponent? {
        return EventApplication.myWelcomeComponent?.getDataBaseComponent()
    }

    override fun getNetWorkComponent(): NetworkComponent? {
        return EventApplication.myWelcomeComponent?.getNetworkComponent()
    }

    override fun showProgress() {
        customProgressbar.visibility = View.VISIBLE
    }

    override fun hidProgress() {
        customProgressbar.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)
        EventApplication.myWelcomeComponent?.getWelcomeComponent(WelcomeModule(this))?.injectMyPresenter(this)
        presenter.initiateSetup()
    }

    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }
}
