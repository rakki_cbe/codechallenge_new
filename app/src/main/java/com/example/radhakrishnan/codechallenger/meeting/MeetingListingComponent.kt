package com.example.radhakrishnan.codechallenger.meeting

import com.example.radhakrishnan.codechallenger.data.EventHelper
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit

@Subcomponent(modules = arrayOf(MeetingModule::class))
interface MeetingListingComponent {
    fun injectData(evet: MeetingListing)
}

@Module
class MeetingModule constructor(val meetingView: MeetingView) {
    @Provides
    fun getEventPresenter(): MeetingListingPresenter {
        return MeetingListingPresenter(meetingView)
    }

    @Provides
    fun getApiHelper(retrofit: Retrofit): EventHelper {
        return EventHelper(retrofit)
    }

    @Provides
    fun getEventAdapter(): Adapter {
        return Adapter()
    }
}
