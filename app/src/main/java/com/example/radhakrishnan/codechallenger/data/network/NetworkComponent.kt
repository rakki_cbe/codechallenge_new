package com.example.radhakrishnan.codechallenger.data.network

import com.example.radhakrishnan.codechallenger.BasePresenter
import dagger.Subcomponent
import javax.inject.Singleton

@Singleton
@Subcomponent(modules = arrayOf(NetworkModule::class))
interface NetworkComponent {
    fun injectMyPresenter(presenter: BasePresenter)

}